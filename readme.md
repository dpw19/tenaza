# Tenaza mecánica

En este repositorio encontrarás todo lo necesario para construir tu propia tenaza, un mecanismo básico que permite relacionar algunos conceptos tales como la transferencia de movimientos, relación de engranes, voltaje, polaridad, etc.

![Tenaza](grafico/completa.jpeg)

Este mecanismo ha vivido diversas modificaciones y siempre está abierto a recibir sugerencias y mejoras ya que se basa en las filosofías de hardware y software libre.

# Estructura del repositorio
En la carpeta [vectoriales](vectoriales) se encuentran las plantillas necesarias:
* [PDF](vectoriales/tenaza.pdf) (Portable Document Format) para corte láser.
* [SVG](vectoriales/tenaza.svg)  (Scalable Vector Graphics) para editar el diseño si necesitas adaptarlo a tus necesidades.

# Materiales
* Corte láser de la plantilla en MDF de 3mm.
* 1 Motoreductor recto
* 4 Tornillos de 1/8" x 3/4" (1/8 de diámetro por 3/4 de largo).
* 2 tornillos M4.
* 4 Rondanas de 1/8.
* 8 tuercas hexagonales de 1/8
* El tubo contenedor de tinta de un boligrafo vacío. También pueden utilzarse dos tornillos de 1/8.
* Cable de calibre AWG 22 o AWG 24.
* 10cm de cable calibre AWG 12, es de los que se utilizan en las instalaciones eléctricas del hogar.

![Piezas MDF](grafico/piezasMdf.jpeg)

![Piezas Extra](grafico/piezasExtra.jpeg)

# Herramientas
* Desarmador plano, de cruz o hexagonal adecuado para los tornillos que conseguiste.
* Pinzas de punta.
* Pinzas de corte.
* Pistola de silicón y una barra de silicón caliente.
* Cautín y soldadura con núcleo de resina. Recuerda que la soldadura sin plomo es más amigable tanto  para el ambiente como para tu salud :)

# Ensamble
## Sección Mecánica
1. Coloca el motor sobre la base y fíjala con los tornillos M4. Los orificios de esta sección tienen la medida adecuada para que los tornillos generen la rosca mientras se están atornillando en la base.  
Debes cortar el sobrante del tornillo que esta más cerca de los engranes.
![Colocación de motor](grafico/montajeMotor.jpeg)

1. Coloca los tres tornillos que van a generar el conmutador para cambiar el sentido de giro del motor.
![Colocación de tornillos](grafico/tornillosConmutadorFrente.jpg)
![Colocación de tornillos](grafico/tornillosConmutadorReverso.jpg)

1. Coloca la tablilla grabada y fíjala con una tuerca y un tornillo.
![Colocación de tablilla](grafico/colocacionTablilla.jpg)

1. Coloca las dos tenazas con los tubos como ejes para que puedan girar, coloca tuercas en la parte de arriba y en la de abajo. En este caso la tuerca hace cuerda sobre los tubos. Aprieta las tuercas de modo que las tenazas giren libremente.
![Colocación de tenazas](grafico/colocacionTenazas.jpg)

1. Finalmente coloca el engrane en la espiga del motorreductor.

## Sección eléctrica
1. Coloca un cable calibre AWG 22 en cada una de las terminales del motorreductor con ayuda del cautín y un poco de soldadura.
![Colocación de cables del motorreductor](grafico/cablesMotorreductor.jpg)

1. Corta dos segmentos de 5cm de cable calibre 22, limpia aproximadamente 1cm de forro en cada uno de los extremos y estaña una de las terminales de cada cable.
![preparacion Cable AWG 22](grafico/cable12estano.jpeg)

1. Pega los dos cables con un poco de silicón caliente sobre la tablilla de modo que la punta no estañada quede entre dos puntas de los tornillos.

1. Introduce los cables del motorreductor por el orificio de la base y sueldalos con los extremos estañados de los cables que están sobre la tablilla.
![Unión de calbles](grafico/unionCables.jpg)

1. Suelda un cable AWG 22 en los dos extremos del conmutador (los tres tornillos que colocaste en el paso dos de la sección mecánica) y otro cable del mismo calibre en el tornillo central del conmutador.
![Conexión de cables del conmutador](grafico/conexionConmutador.jpg)

# Listo!!!

Ahora lo único que debes hacer es conectar una fuente de energía en las terminales y mover el conmutador para que el mecanismo abra y cierre las tenazas.

El motorreductor funciona bien con voltajes entre los 5V y 9V, procura mantener el voltaje en ese rango.

Disfruta y comparte con tus amistades!!!

Mira los videos de funcionamiento
* [Video 1](grafico/funcionamiento_sinSonido.mp4)
* [Video 2](grafico/funcionamiento2_sinSonido.mp4)

# Dudas sugerencias
Este repositorio es tipo git, lo que te permite colaborar activamente o simlemente clonarlo en tu equipo personal.

Aprende más sobre [git](https://git-scm.com/) en el sitio oficial.

O bien escribe tus comentarios, dudas, aportes

Pavel E. Vázquez Mtz. pavel_e@riseup.net
